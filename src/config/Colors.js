const Colors = {
    blackPearl: '#20252c',
    charade: '#272c35',
    zircon: '#495464',
    white: '#fff',
    green: '#81b214',
    red: '#ec0101',
    black: 'rgba(0,0,0,0.2)'
}

export default Colors;
