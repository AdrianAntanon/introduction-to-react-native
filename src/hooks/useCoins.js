import { useState, useEffect } from 'react';
import { Http } from '../libs/';

const URL = 'https://api.coinlore.net/api/tickers/';

const useCoins = () => {
    const [coins, setCoins] = useState([]);
    useEffect(() => {
        (async () => {
            const response = await Http.instance.get(
                URL,
            );
            setCoins(response.data);
        })();
    }, []);
    return [coins];
};

export default useCoins;