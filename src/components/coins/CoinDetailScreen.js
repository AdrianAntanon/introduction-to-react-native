import React, { useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    SectionList
} from 'react-native';
import Colors from '../../config/Colors';

const CoinDetailScreen = ({ route, navigation }) => {
    const { coin } = route.params;


    const getSymbolImage = () => `https://c1.coinlore.com/img/25x25/${coin.nameid}.png`;

    useEffect(() => {
        navigation.setOptions({ title: coin.symbol })
    }, []);

    const getSections = () => {
        const sections = [
            {
                title: 'Price',
                data: [coin.price_usd],
            },
            {
                title: 'Market Cap',
                data: [coin.market_cap_usd],
            },
            {
                title: 'Change 24h',
                data: [coin.percent_change_24h],
            },
        ];
        return sections;
    }

    return (
        <View style={styles.container} >
            <View style={styles.subHeader} >
                <Image style={styles.iconImg} source={{ uri: getSymbolImage(coin.nameid) }} />
                <Text style={styles.titleText} > {coin.nameid} </Text>
            </View>
            <SectionList
                sections={getSections()}
                keyExtractor={(item) => item.id}
                renderSectionHeader={({ section }) => (
                    <View style={styles.sectionItem}>
                        <Text style={styles.itemText}>
                            {section.title}
                        </Text>
                    </View>
                )}
                renderItem={({ item }) => (
                    <View style={styles.sectionHeader}>
                        <Text style={styles.sectionText}>
                            {item}
                        </Text>
                    </View>
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.charade,
        flex: 1,
    },
    subHeader: {
        backgroundColor: Colors.black,
        padding: 16,
        flexDirection: "row",
    },
    titleText: {
        fontSize: 16,
        color: Colors.white,
        fontWeight: "bold",
        marginLeft: 8,
    },
    iconImg: {
        width: 25,
        height: 25,
    },
    sectionHeader: {
        backgroundColor: Colors.black,
        padding: 8,
    },
    sectionItem: {
        padding: 8
    },
    itemText: {
        color: Colors.white,
        fontSize: 14
    },
    sectionText: {
        color: Colors.white,
        fontSize: 14,
        fontWeight: "bold",
    },

});

export default CoinDetailScreen;