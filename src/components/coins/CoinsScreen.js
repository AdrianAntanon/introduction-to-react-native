import React, { useEffect } from 'react';
import {
    ActivityIndicator,
    View,
    Text,
    StyleSheet,
    Pressable,
    FlatList
} from 'react-native';

import CoinsItem from './CoinsItem';

import { Http } from '../../libs';
import { useCoins } from '../../hooks';
import Colors from '../../config/Colors';

// URL API
// const URL = 'https://api.coinlore.net/api/tickers/';

const CoinsScreen = (props) => {

    // Usamos nuestro custom hook
    const [coins, setCoins] = useCoins();

    // Manejando el evento que se ejecuta al pulsar
    const handlePress = (coin) => {
        // console.log('go to detail', { navigation });
        props.navigation.navigate('CoinDetail', { coin })
    };

    return (
        <View style={styles.container} >
            <Text style={styles.titleText} >Coins Screen</Text>
            {coins !== undefined ? (
                <FlatList
                    data={coins}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => <CoinsItem {...item} onPress={() => handlePress(item)} />}
                />
            ) : (
                    <ActivityIndicator style={styles.loader} color="#000" size="large" />
                )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.charade,
    },
    titleText: {
        color: Colors.white,
        textAlign: "center",
        fontSize: 30,
        marginTop: 10,
    },
    btn: {
        padding: 8,
        backgroundColor: "green",
        borderRadius: 8,
        margin: 16,
    },
    btnText: {
        color: "#fff",
        textAlign: "center",
    },
    loader: {
        marginVertical: 60,
    }
});

export default CoinsScreen;


