import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { View, StyleSheet } from 'react-native';

import CoinDetailScreen from './CoinDetailScreen';
import CoinsScreen from './CoinsScreen';

import Colors from '../../config/Colors';


const Stack = createStackNavigator();


const CoinsStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.blackPearl,
                    shadowColor: Colors.blackPearl
                },
                headerTintColor: Colors.white
            }}
        >
            <Stack.Screen
                name="Coins"
                component={CoinsScreen} />
            <Stack.Screen
                name="CoinDetail"
                component={CoinDetailScreen} />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    container: {

    }
});

export default CoinsStack;