import React from 'react';
import { View, Text, StyleSheet, Image, Platform, Pressable } from 'react-native';

import Colors from '../../config/Colors';
import ArrowUp from '../../assets/arrow_up.png';
import ArrowDown from '../../assets/arrow_down.png';


const CoinsItem = ({ onPress, ...otherProps }) => {
    const { name, symbol, percent_change_1h, price_usd } = otherProps;

    return (
        <Pressable onPress={onPress} style={styles.container} >
            <View style={styles.column} >
                <Text style={styles.symbolText} > {name} </Text>
                <Text style={styles.nameText} > {symbol} </Text>
            </View>
            <View style={styles.column}>
                <Text style={styles.priceText}>{`$${price_usd}`}</Text>
                <View style={percent_change_1h > 0 ? styles.rowGreen : styles.rowRed}>
                    <Text style={styles.percentText}>{percent_change_1h}</Text>
                </View>
            </View>
        </Pressable>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 16,
        borderBottomColor: Colors.zircon,
        borderBottomWidth: 1,
        marginLeft: Platform.OS === 'ios' ? 16 : 0,
    },
    column: {
        flexDirection: 'column',
    },
    rowGreen: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 5,
        width: 70,
        height: 20,
        backgroundColor: Colors.green,
    },
    rowRed: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 5,
        width: 70,
        height: 20,
        backgroundColor: Colors.red,
    },
    symbolText: {
        color: Colors.white,
        fontWeight: 'bold',
        fontSize: 16,
        marginRight: 12,
    },
    nameText: {
        color: Colors.white,
        fontSize: 14,
        marginTop: 2,
    },
    priceText: {
        color: Colors.white,
        fontSize: 14,
        marginLeft: 16,
    },
    percentText: {
        color: Colors.white,
        fontSize: 12,
        textAlign: 'right',
        width: 60,
        marginTop: 1,
    },
    imageIcon: {
        width: 15,
        height: 15,
        marginRight: 8,
    },
});

export default CoinsItem;